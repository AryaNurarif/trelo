<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="public/css/style.css" />
	<!-- Bootstrap CSS -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" />
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
</head>

<body>

	<style>
		body {
			font-family: 'Poppins';
			font-size: 22px;
		}
	</style>
	<div class="container">
		<div class="brow">
			<nav class="navbar navbar-expand-lg navbar-light justifi-content-end">
				<div class="judul">
					<h2>Cek.La</h2>
				</div>
				<ul class="navbar-nav">
					<div class="nav-link-light">Login</div>
					</li>
					<li class="nav-item">
						<div class="nav-link-dark">Signup</a>
					</li>
				</ul>
			</nav>
		</div>
		<div class="margin">
			<div class="atur">
				<div class="manto">Create Your link</div>
				<div class="create">Make you free and as you like link</div>

			</div>
			<div class="ada">
				<div>
					<input class="place" placeholder="Put your long URL here">
					<img class="monas" src="public/css/link-45deg.svg">
					<button class="short">Shorten</button>
				</div>
			</div>
		</div>


		<div class="custome">
			<div class="xyz">
				<i class="fas fa-link fa-4x"></i>
			</div>
			<div class="style"> Custome Link </div>
			<div class="paseo">Vary the link, and make the check your link indentity
			</div>
		</div>

		<div class="pertemuan">
			<div class="yzx">
				<i class="fab fa-stackpath fa-4x"></i>
			</div>
			<div class="style">Track Audience</div>
			<div class="paseo">
				You can see how much traffic your audience is getting into your check.la link.
			</div>

		</div>
		<div class="buku">
			<div class="bca">
				<i class="fas fa-store fa-3x"></i>
			</div>
			<div class="style">Easy Product Marketing</div>
			<div class="paseo">
				Helping your website or social media marketing easier and more interesting in marketing your products.
			</div>
		</div>

		<div class="kaki">
			<div class="tangan">Cek.la</div>
			<div class="jari">
				<div class="hijau">
					Bantuan Pelanggan</div>
				<div class=mulut>
					<div class="awas">Pengaduan</div>
					<div class="jur">Syarat & Ketentuan</div>
					<a>Kebijakan Privasi</a>
					</ul>
				</div>
			</div>
			<div class="badan">
				<div class="kuping">Tentang Kami</div>
				<div class="kuning">
					Company File
				</div>
			</div>
			<div class="kami">
				<div class="telinga">Hubungi Kami</div>
				<div class="merah">
					<ul class="list-unstyled">
						<li>
							<i class="fab fa-whatsapp"></i> 081 18172299
						</li>
						<li>
							<i class="fas fa-phone-alt"></i> 021 791888868
						</li>
						<li>
							<p class="fas fa-envelope"></p> info@Cekla.com
						</li>
					</ul>

				</div>
			</div>
		</div>

	</div>

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/js/all.min.js"></script>

	<!-- Option 2: Separate Popper and Bootstrap JS -->
	<!--
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
      -->
</body>

</html>